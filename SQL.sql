CREATE DATABASE mondaisyu;
USE mondaisyu;
CREATE TABLE item_category(category_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, category_name VARCHAR(256) NOT NULL);
SELECT * FROM item_category;

CREATE TABLE item(item_id INT PRIMARY KEY AUTO_INCREMENT, item_name VARCHAR(256) NOT NULL, item_price INT NOT NULL, category_id INT);
SELECT * FROM item;

INSERT INTO item_category VALUES(1,'家具');
INSERT INTO item_category VALUES(2,'食品');
INSERT INTO item_category VALUES(3,'本');
SELECT * FROM item_category;

INSERT INTO item(item_name,item_price,category_id)VALUES('堅牢な机',3000,1);
INSERT INTO item(item_name,item_price,category_id)VALUES('生焼け肉',50,2);
INSERT INTO item(item_name,item_price,category_id)VALUES('スッキリわかるJAVA入門',3000,3);
INSERT INTO item(item_name,item_price,category_id)VALUES('お洒落な椅子',2000,1);
INSERT INTO item(item_name,item_price,category_id)VALUES('こんがり肉',500,2);
INSERT INTO item(item_name,item_price,category_id)VALUES('書き方ドリルSQL',2500,3);
INSERT INTO item(item_name,item_price,category_id)VALUES('フワフワのベッド',30000,1);
INSERT INTO item(item_name,item_price,category_id)VALUES('ミラノ風ドリア',300,2);

SELECT item_name, item_price FROM item WHERE category_id = 1;
SELECT item_name,item_price FROM item WHERE item_price >= 1000;

DELETE FROM item WHERE item_id = 9;
DELETE FROM item WHERE item_id = 10;

SELECT item_name,item_price FROM item WHERE item_name LIKE '%肉%';

SELECT 
item_category.category_name,
item.item_id,
item.item_name,
item.item_price
FROM
item_category
INNER JOIN
item
ON
item_category.category_id = item.category_id;

SELECT 
*
FROM 
   
  item_category
  INNER JOIN
  item
  
  ON
  item_category.category_id = item.category_id;

SELECT
item_category.category_name,
sum(item_price) totalprice
FROM 
item
INNER JOIN
item_category
ON
item_category.category_id = item.category_id
GROUP BY
    item.category_id
ORDER BY
	totalprice desc;